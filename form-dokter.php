<?php
	session_start();
	if(empty($_SESSION['username'])) {
	header ("location:form-login-user.php");
	}
?>
<html>
	<head>
		<title>Rumah Sakit Cepat Sembuh</title>
		<link rel="stylesheet" type="text/css" href="tampilan/style.css"/>
	</head>
	<body background="image/bg.jpg">
		<div id='header'>

		</div>
		<div id='menu'>
			<?php
				include "menu.php";
			?>
		</div>
		<div id='paper'>
		<center>
			<p>Selamat Datang, <?php echo $_SESSION['username']; ?> | <a href="logout-user.php" onclick="return confirm('Apakah Anda yakin akan keluar?')">Log Out</a></p><br><br>
			<font color="red"><b>Formulir Data Dokter</b></font>
			<br><br>
		</center>
		<form action="simpan-dokter.php" method="post">
		<?php
		if(isset($_GET['kode'])) {
			include "koneksi.php";
			$kode=$_GET['kode'];
			
			$query=mysql_query("SELECT dokter.kd_dokter, dokter.kd_poli, dokter.hari, dokter.kd_user, dokter.nm_dokter, dokter.sip, dokter.tmpt_lahir, dokter.no_tlp, dokter.alamat, login.username, login.password FROM dokter, login WHERE dokter.kd_user=login.kd_user AND kd_dokter='$kode'");
			$row=mysql_fetch_array($query);
			$hari=$row['hari'];
			?>
			<table align="center">
				<tr>
					<td>Kode Dokter</td>
					<td>:</td>
					<td><input type="text" name="kode" required value="<?php echo $kode; ?>" readonly /></td>
				</tr>
				<tr>
					<td>Nama Dokter</td>
					<td>:</td>
					<td><input type="text" name="nm_dokter" required value="<?php echo $row['nm_dokter']; ?>" maxlength="50"/></td>
				</tr>
				<tr>
					<td>Poliklinik</td>
					<td>:</td>
					<td><select name="kd_poli" required>
					<?php
						$poli=mysql_query("SELECT * FROM poliklinik");
						while ($plk=mysql_fetch_array($poli)) {
						if($plk['kd_poli']==$row['kd_poli']) {
							echo "<option value=$plk[kd_poli] selected>$plk[nm_poli]</option>";
						} else {
							echo "<option value=$plk[kd_poli]>$plk[nm_poli]</option>";
						}
						}
					?>
					</select>
					</td>
				</tr>
				<tr>
					<td>Hari</td>
					<td>:</td>
					<td><select name="hari" required>
						<?php
							if($hari=="Senin") {
							echo "<option value='$hari'>$hari</option>
							<option value='Selasa'>Selasa </option>
							<option value='Rabu'>Rabu</option>
							<option value='Kamis'>Kamis</option>
							<option value='Jumat'>Jumat </option>
							<option value='Sabtu'>Sabtu</option>
							<option value='Minggu'>Minggu</option>";
							} else if($hari=="Selasa") {
							echo "<option value='$hari'>$hari</option>
							<option value='Senin'>Senin </option>
							<option value='Rabu'>Rabu</option>
							<option value='Kamis'>Kamis</option>
							<option value='Jumat'>Jumat </option>
							<option value='Sabtu'>Sabtu</option>
							<option value='Minggu'>Minggu </option>";
							} else if($hari=="Rabu") {
							echo "<option value='$hari'>$hari</option>
							<option value='Senin'>Senin </option>
							<option value='Selasa'>Selasa </option>
							<option value='Kamis'>Kamis</option>
							<option value='Jumat'>Jumat </option>
							<option value='Sabtu'>Sabtu</option>
							<option value='Minggu'>Minggu </option>";
							} else if($hari=="Kamis") {
							echo "<option value='$hari'>$hari</option>
							<option value='Senin'>Senin </option>
							<option value='Selasa'>Selasa </option>
							<option value='Rabu'>Rabu</option>
							<option value='Jumat'>Jumat </option>
							<option value='Sabtu'>Sabtu</option>
							<option value='Minggu'>Minggu </option>";
							} else if($hari=="Jumat") {
							echo "<option value='$hari'>$hari</option>
							<option value='Senin'>Senin </option>
							<option value='Selasa'>Selasa </option>
							<option value='Rabu'>Rabu</option>
							<option value='Kamis'>Kamis</option>
							<option value='Sabtu'>Sabtu</option>
							<option value='Minggu'>Minggu </option>";
							} else if($hari=="Sabtu") {
							echo "<option value='$hari'>$hari</option>
							<option value='Senin'>Senin </option>
							<option value='Selasa'>Selasa </option>
							<option value='Rabu'>Rabu</option>
							<option value='Kamis'>Kamis</option>
							<option value='Jumat'>Jumat </option>
							<option value='Minggu'>Minggu </option>";
							} else if($hari=="Minggu") {
							echo "<option value='$hari'>$hari</option>
							<option value='Senin'>Senin </option>
							<option value='Selasa'>Selasa </option>
							<option value='Rabu'>Rabu</option>
							<option value='Kamis'>Kamis</option>
							<option value='Jumat'>Jumat </option>
							<option value='Sabtu'>Sabtu </option>";
							}
						?>
						</select></td>
				</tr>
				<tr>
					<td>Alamat</td>
					<td>:</td>
					<td><textarea maxlength="100" required name="alamat" style="max-width:175px;width:175px;max-height:75px;height:75px;" ><?php echo $row['alamat']; ?></textarea></td>
				</tr>
				<tr>
					<td>Tempat Lahir</td>
					<td>:</td>
					<td><input type="text" name="tmpt_lahir" required maxlength="30" value="<?php echo $row['tmpt_lahir']; ?>" /></td>
				</tr>
				<tr>
					<td>Kode User</td>
					<td>:</td>
					<td><input type="text" name="kd_user" readonly required value="<?php echo $row['kd_user']; ?>">
					</td>
				</tr>
				<tr>
					<td>No Telp</td>
					<td>:</td>
					<td><input type="number" name="no_tlp" required value="<?php echo $row['no_tlp']; ?>" maxlength="30"/></td>
				</tr>
				<tr>
					<td>No SIP</td>
					<td>:</td>
					<td><input type="text" name="sip" required value="<?php echo $row['sip']; ?>" maxlength="50"/></td>
				</tr>
				<tr>
					<td>Username</td>
					<td>:</td>
					<td><input type="text" name="username" readonly required value="<?php echo $row['username']; ?>">
					</td>
				</tr>
				<tr>
					<td>Password</td>
					<td>:</td>
					<td><input type="text" name="password" readonly required value="<?php echo $row['password']; ?>">
					</td>
				</tr>
				<tr>
					<input type="hidden" name="status" value="1" />
				</tr>
				<tr>
					<td><input type="submit" value="Ubah" /></td>
				</tr>
			</table>
			<?php
		} else {
			include "koneksi.php";
			//$kode=$_GET['kd_user'];
		?>
		<table align="center">
			<tr>
				<td>Nama Dokter</td>
				<td>:</td>
				<td><input type="text" maxlength="50" name="nm_dokter" required></td>
			</tr>
			<tr>
				<td>Poliklinik</td>
				<td>:</td>
				<?php
					include "koneksi.php";
					$poli=mysql_query("SELECT * FROM poliklinik");
				?>
				<td><select name="kd_poli" required>
					<?php
						while($plk=mysql_fetch_array($poli)) {
							echo "<option value='".$plk['kd_poli']."'>".$plk['nm_poli']."</option>";
						}
					?>
				</select>
				</td>
			</tr>
			<tr>
				<td>Hari</td>
				<td>:</td>
				<td><select name="hari" required>
					<option value="Senin">Senin</option>
					<option value="Selasa">Selasa</option>
					<option value="Rabu">Rabu</option>
					<option value="Kamis">Kamis</option>
					<option value="Jumat">Jumat</option>
					<option value="Sabtu">Sabtu</option>
					<option value="Minggu">Minggu</option>
				</select></td>
			</tr>
			<tr>
				<td>Alamat</td>
				<td>:</td>
				<td><textarea required name="alamat"  maxlength="100" style="max-width:175px;width:175px;max-height:75px;height:75px;" ></textarea></td>
			</tr>
			<tr>
				<td>Tempat Lahir</td>
				<td>:</td>
				<td><input type="text" maxlength="30"  name="tmpt_lahir" required></td>
			</tr>
			<tr>
				<td>No Telp</td>
				<td>:</td>
				<td><input type="number" maxlength="30" name="no_tlp" required></td>
			</tr>
			<tr>
				<td>No SIP</td>
				<td>:</td>
				<td><input type="text" maxlength="50" name="sip" required></td>
			</tr>
			<tr>
				<td>Username</td>
				<td>:</td>
				<td><input type="text" maxlength="50" name="username" required></td>
			</tr>
			<tr>
				<td>Password</td>
				<td>:</td>
				<td><input type="text" maxlength="50" name="password" required></td>
			</tr>
			<tr>
				<input type="hidden" value="0" name="status">
			</tr>
			<tr>
				<td><input type="submit" value="Simpan"><input type="reset" value="Kosongkan"></td>
			</tr>
		</table> 
		<?php
		}
		?>
		</form>
		<form method=post>
			<center>
			<input type=button value=Kembali onclick=self.history.back() />
			</center>
		</form>
		</div>
		<div id='footer'>
			<center> Copyright &copy; ASRINI RPL 07</center>
		</div>
	</body>
</html>